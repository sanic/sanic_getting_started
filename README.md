## Getting Started - sanic python

Seguindo o tutorial da documentação do sanic python. Disponível na url: https://sanic.readthedocs.io/en/latest/index.html

## Instalando dependências
Dependências: é necessário ter o python na versão 3.5 ou superior.

- Crie uma virtualenv com python3 utilizando o seguinte comando: `python3 -m venv sanic`
- Ative a virtualenv `sanic`: `. sanic/bin/activate`

- Instale as dependências contidas no arquivo requirements.txt: `pip install -r requirements.txt`

## Executando o projeto
- Com a virtualenv ativa, execute o servidor: `python main.py`
- Para ver o resultado acesse a ulr: `http://0.0.0.0:8000`

