from sanic import Sanic
from sanic.response import text

app = Sanic()

# ---- The add_route method ---- #
async def handler1(request):
    return text('OK')

async def handler2(request, name):
    return text('Folder - {}'.format(name))

async def person_handler2(request, name):
    return text('Person - {}'.format(name))

# adicionando uma rota para cada função
app.add_route(handler1, '/test')
app.add_route(handler2, '/folder/<name>')
app.add_route(person_handler2, '/person/<name:[A-z]>', methods=['GET'])



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
