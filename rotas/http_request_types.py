from sanic import Sanic
from sanic.response import json, text

app = Sanic()

# ---- HTTP request types ---- #
@app.route('/post', methods=['POST'])
async def post_handler(request):
    return text('POST request - {}'.format(request.json))

@app.route('/get', methods=['GET'], host='google.com')
async def get_handler(request):
    return text('GET request - {}'.format(request.args))

# if the host header doesn't match example.com, this route will be used
@app.route('/get', methods=['GET'])
async def get_handler(request):
    return text('GET request in default- {}'.format(request.args))

#decorators
@app.post('/post2')
async def post_handler(request):
    return text('POST request - {}'.format(request.json))

@app.get('/get2')
async def get_handler(request):
    return text('GET request - {}'.format(request.args))




if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
