from sanic import Sanic, Blueprint
from sanic.response import text

# --- About strict_slashes ---- #

app = Sanic('test_route_strict_slash', strict_slashes=True)

@app.get('/get', strict_slashes=False)
def handler(request):
    return text('ok')


bp = Blueprint('test_bp_strict_slash', strict_slashes=True)

@bp.get('/bp/get', strict_slashes=False)
def handler(request):
    return text('ok')

app.blueprint(bp)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
