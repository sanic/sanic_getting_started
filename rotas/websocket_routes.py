from sanic import Sanic

app = Sanic()

@app.websocket('/feed')
async def feed(request, ws):
    while True:
        data = 'hello!'
        print('Sending: ', + data)
        await ws.send(data)
        data = await ws.recv()
        print('Received: ' + data)


# ou usar o app.add_websocket_route

# async def feed(request, ws):
#     pass

# app.add_websocket_route(feed, '/feed')


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
