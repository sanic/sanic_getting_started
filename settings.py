from sanic import Sanic

app = Sanic('myapp')
# app.config.DB_NAME = 'appdb'
# app.config.DB_USER = 'appuser'

db_settings = {
    'DB_HOST': 'localhost',
    'DB_NAME': 'appdb',
    'DB_USER': 'appuser'
}
app.config.update(db_settings)



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
